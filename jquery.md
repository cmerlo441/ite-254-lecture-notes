# Using jQuery

Remember to reference [jQuery.com](jquery.com) as necessary for help

jQuery is an extension on JavaScript, so everything you know about JavaScript still applies

## CDN

Go to the download link on [jQuery.com](jquery.com), scroll down to "Using jQuery with a CDN", and copy one of those links into the head of a web document.

## Create a jQuery Script

Somewhere in your HTML and/or PHP page, add this code:

```html
<script type="text/javascript">
    $(function(){
        // jQuery code goes here
    })
</script>
```

## Selectors

jQuery makes the `$` selector available, which works like a function.  Pass a string to the `$` function that represents some object (or objects, but we'll talk about that later) of the DOM, and then you can call functions on the result.

Example:
```javascript
$('div#div1').css('background-color','blue');
```

That calls the `css()` function on the `div` returned by the selector.

<hr>

What we did in class on Wednesday 2019-11-27

## The `display` attribute in CSS

If you can see something on a web page, its CSS `display` attribute is most likely either set to `block` or `inline`.  Things like `div`s are block-level elements, and things like `span`s are inline elements.

If you change this attribute to `none`, the object doesn't appear.

Using the jQuery functions `hide()`, `show()`, and others, we can make cool stuff happen like this:

```html
<html>
<head>
<title>This is a web page</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<style type="text/css">
.blue {
	background-color: #8888ff;
}
</style>
</head>
<body>
	<div id="links">
		<a href="javascript:void(0)" class="toggle" id="a1">Click me!</a>
		<a href="javascript:void(0)" class="toggle" id="a2">Click me too!</a>
	</div>

	<div id="div1" class="class1">
	This is div 1
	</div>

	<div id="div2" class="class1">
	This is <span id="mySpan">div 2</span>.  Isn't that cool?
	</div>

	<div id="div3" class="class2">
	This is div3
	</div>

	<div id="div4" class="class2">
	This is div 4
	</div>

	<script type="text/javascript">
	$(function(){
		//console.log( $('div.class1') );
		//console.log( $('div.class1').css('display') );
		//console.log( $('span#mySpan').css('display') );
		//$('span#mySpan').css('display','block');
		$('div.class1').css('display','none');

		$('a#a1').click(function(){
			$('div.class1').slideToggle(2000);
		});

		$('a#a2').click(function(){
			$('span#mySpan').fadeToggle();
		});

		$('a.toggle').click(function(){
			console.log(getTime());
		});

	})
	</script>

</body>
</html>
```

<hr>

What we did in class on 2019-12-02

## AJAX

Today's lesson was on Asynchronous Javascript and XML.  Students edited the code in [main.html](../main.html) to add a `<script>` tag and the following JavaScript code to have the contents of `secondary.txt` appear inside the empty div tag:

```html
<script type="text/javascript">
$('a#ajax'.click(function(){
	$.get('secondary.txt',
		{ 'value1' : 5, 'value2' : 10 },
		function(data) {
			$('div#stuff').html(data);
		}
	);
}));
</script>
```

Afterward, by applying the same logic to a call to [ajax.php](../ajax.php), students were able to change the variable's values and see different things appear, and then change the call to `$.get()` to a call to `$.post()` and change what appears in the div even more.

<hr />

What we did in class on Monday 2019-12-09

## A Common AJAX Bug

Look at this file, `ajax-bug.html`:

```html
<html>
    <head>
        <meta charset="utf-8" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    </head>
    <body>
        <a href="javascript:void(0)" class="link" id="link1">hi</a>
        <a href="javascript:void(0)" class="link" id="link2">hi</a>
        <a href="javascript:void(0)" class="link" id="link3">hi</a>
        <div id="more_links"></div>
        <div id="results"></div>
        
        <script type="text/javascript">
            $(function(){
                $('a.link').on('click', function(){
                    //console.log('here');
                    var id = $(this).attr('id');
                    //console.log(id);
                    $('div#results').html(id);
                    //$('div').html(id);
                    $.get('ajax-bug-2.html',
                      function(data){
                        $('div#more_links').html(data);
                    }
                    )
                });
            })
        </script>
        
    </body>
</html>
```

This file uses AJAX to load in `ajax-bug-2.html`, seen here:

```html
<a href="javascript:void(0)" class="link" id="this_came_from_ajax">Hello</a>
```

Note that the class of the `<a>` tag that gets AJAXed in is the same as the class of all the `<a>` tags in the "host" file.  The bug is that the the link that gets AJAXed in won't respond to the jQuery click event, because the event handler was bound before the link was part of the document.

The fix, as seen here, is to call the jQuery version of `on()` that has *three* parameters, and to call it on some DOM object farther up the DOM than your event sources -- in this case, we call `on()` on a `div` that contains all the links:

```html
<html>
    <head>
        <meta charset="utf-8" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    </head>
    <body>
        <div id="thediv">
            <a href="javascript:void(0)" class="link" id="link1">hi</a>
            <a href="javascript:void(0)" class="link" id="link2">hi</a>
            <a href="javascript:void(0)" class="link" id="link3">hi</a>
            <div id="more_links"></div>
            <div id="results"></div>
        </div>  <!-- closes thediv -->
        
        <script type="text/javascript">
            $(function(){
                $('#thediv').on('click', 'a', function(event){
                    //console.log(event);
                    //console.log( event.target);
                    //var id = event.target.attr('id');
                    //console.log(id);
                    var link = event.target;
                    console.log(link);
                    console.log( $(link).attr('id'))
                    $.get('ajax-bug-2.html',
                          function(data){
                        $('div#results').html(data);
                    })
                });
            })
        </script>
        
    </body>
</html>
```

Now, when the new link is AJAXed in, the event will be recognized and fire because it's part of `#thediv`.