<?php

if( isset( $_POST[ 'value' ] ) ) {
  print "<p>You called me via POST and sent this value: {$_POST[ 'value' ]}</p>\n";
} else if( isset( $_GET[ 'value' ] ) ) {
  print "<p>You called me via GET and sent this value: {$_GET[ 'value' ]}</p>\n";
}

?>