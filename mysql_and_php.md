# Using MySQL with PHP

What we did in ITE 254 GA on Monday October 14th

Prof. C. Merlo

## Create a Database Table

- Copy the file `movies.sql` from my `public_html` directory into your home directory
- Import it into MySQL with this command:

    `$ mysql -p -D xxx1234 < movies.sql`

    Where `xxx1234` is your username on Stargate

## Create a Database Object in PHP

- Use the `mysqli` class' constructor to create a database connection object called `$db`:

    ```php
    $db = new mysqli( HOSTNAME, USERNAME, PASSWORD );
    ```

    replacing `HOSTNAME` with `'localhost'` and both `USERNAME` and `PASSWORD` with your username on Stargate.  Notice that these all must be strings, and so surrounded with either single or double quotes.

- Call the `select_db()` function on the newly-created database object to change to your database:

    ```php
    $db->select_db( 'xxx1234' );
    ```

## Create a Query

I tend to create a variable to store my query string.  As you will see later this semester, queries will get more complicated as we go on, and so I've found that this technique helps me keep track of what I'm doing.

Run the query `show tables` at the mysql prompt, and you should see:

```
+-------------------+
| Tables_in_xxx1234 |
+-------------------+
| movies            |
| toppings          |
+-------------------+
2 rows in set (0.00 sec)
```

Notice that there are two rows in this result set, and one field.  We will see this information again later.

### List all the tables in the database

- Create a variable to store the query string:

    ```php
    $show_tables_query = 'show tables';
    ```

- Call the `query()` function on the database object, passing this query in as a parameter.  Store the result in a variable:

    ```php
    $show_tables_result = $db->query( $show_tables_query );
    ```

- Use the `print_r` function to see what's in the result object:

    ```php
    print "<pre>\n";
    print_r( $show_tables_result );
    print "</pre>\n";
    ```

    This should generate output like:

    ```
    mysqli_result Object
    (
        [current_field] => 0
        [field_count] => 1
        [lengths] => 
        [num_rows] => 2
        [type] => 0
    )
    ```

    Notice that the field count and number of rows are as we expected earlier.

### List all the movies in the `movies` table

It will be more interesting to get data out of the table we created at the start of class.  The query to see all the movies we created is `select * from movies`.

```php
$movies_query = 'select * from movies';
$movies_result = $db->query( $movies_query );
print "<php>\n";
print_r( $movies_result );
print "</php>\n";
```

The output here should be:
```
mysqli_result Object
(
    [current_field] => 0
    [field_count] => 3
    [lengths] => 
    [num_rows] => 4
    [type] => 0
)
```

Now there are four rows in the result, and each one has three fields.  To get one particular row, we call the `fetch_object()` method on the *result*, not on the database object:

```php
$row = $movies_result->fetch_object();
print "<pre\n";
print_r( $row );
print "</pre>\n";
```

and get output like this:
```
stdClass Object
(
    [id] => 1
    [title] => The Godfather
    [year] => 1972
)
```

<hr>

What we did on Wednesday, October 16th

## The `$row` object

Notice that the object we get from the `fetch_object()` method contains one key-value pair for each field in the table.  Keys are named after the fields, and values are the values for that row.

First thing we did was to copy this code:

```php
$row = $movies_result->fetch_object();
print "<pre\n";
print_r( $row );
print "</pre>\n";
```

and paste it so that we get the second row of the table.

## PHP Object Notation

As we've seen, use the `->` operator (or the "arrow operator") to retrieve one piece of data from an object.

So, to get just the title from the row, use `$row->title`.  We did this and stored the title in a variable:

```php
$row = $movies_result->fetch_object();
$title = $row->title
```

With that we can then print the title or do whatever we need to.

## Looping Over the Entire Result Set
That `$movies_result` object gives us access to all of the rows from the query.  Using the `fetch_object()` function that way gives us the next row of data each time we call it.

What's interesting to know is that that function returns `false` when it's out of rows, so we can call it in a loop:

```php
while( $row = $movies_result->fetch_object() ) {
    // do cool stuff with $row
}
```

We used this to create a bulleted list of all the titles:

```php
print "<ul>\n";
while( $row = $movies_result->fetch_object() ) {
    $title = $row->title;
    print "<li>$title</li>\n";
}
print "</ul>\n";
```

## Uniquely Identifying List Items

Remember that the RDBMS assigned a unique ID to each row in the table.  Those IDs are in the result set from the query.  We can (and should) use them to provide unique IDs to each `<li>` in the HTML we generate:

```php
print "<ul>\n";
while( $row = $movies_result->fetch_object() ) {
    $title = $row->title;
    $id = $row->id;
    print "<li id=\"$id\">$title</li>\n";
}
print "</ul>\n";
```

If you go back and add the clause `"order by title"` to the end of the query, you will see that the movies will be listed in a different order -- but that the IDs generated by the RDBMS stay with their movies!  Use "Show Source" to see that the HTML generated by our PHP reflects this.

<hr>

What we did on Monday, October 21st

## PHPMyAdmin

We discussed the PHPMyAdmin tool available at [http://stargate.ncc.edu/phpmyadmin](http://stargate.ncc.edu/phpmyadmin) and how it can be used to easily edit a table.

## `.htpasswd`

- We created a file called `.htpasswd` in the web directory
- We placed `<?php` and `?>` at the top and bottom
- We copied the database connection and selection code from `movies1.php` into the new file
- We commented out the copied code within `movies1.php`
- We used the `include()` function to have `movies1.php` run the PHP code inside `.htpasswd` and confirmed that the program still runs the way it used to

<hr>

What we did on Wednesday, October 30th

## Updating a Database

Today's challenge was to write some PHP code that inserts data into a database.

### How to Insert Data Into a Database
The SQL to insert data into a database looks like this:

```sql
insert into tablename( field1, field2, ... )
    values ( field1value, field2value, ... );
```
Note that the values to be inserted really ought to be surrounded with double quotes - this avoids needing backslashes in a lot of cases.

### Searching a Database Table for a Partial String
Using the `like` and `%` operators we can write a query that searches for a *substring* within a field:

```sql
/* Only return an exact match for "string": */
select * from tablename where field = "string";

/* Return any string that starts with "string" like "strings are cool": */
select * from tablename where field like "string%";

/* Return anything that ends with "string" like "this is a string": */
select * from tablename where field like "%string";

/* Return anything that contains a string like "this string is cool": */
select * from tablename where field like "%string%";
```

Notice that these matches are case-insensitive.

### The Challenge

Create a form with just a textbox and a button.  Have the page reload itself when the button is pressed by using `PHP_SELF` in the `action` attribute of the `form` tag:

```php
<form action = "<?php echo $_SERVER[ 'PHP_SELF' ]; ?>">
```

Remember to add a `method` attribute as well.

When form data has been sent to the page, do two things:

1. Process the value in the textbox by searching for all movies in the movies table whose titles contain the entered text
2. Add a row to a database table that keeps track of searches.  Each row of this table should contain an auto-incrementing ID number, a movie's ID number, and the date and time when the query occured.  Use the `datetime` data type in your table, and use the `date()` function in PHP to generate a string like `"2019-10-30 15:22:07"`.

<hr>

What we did Wednesday, November 6th

## Hit Counter

We wrote this PHP file called `count_updater.php` to update a database every time it ran.  Notice that the PHP first checks to see if there's a row in the database representing the current page, and creates one if it isn't there.

```php
<?php

$this_page = $_SERVER[ 'PHP_SELF' ];

$search_query = 'select id, count '
	. 'from page_count '
	. "where pagename = \"$this_page\"";
$search_result = $db->query( $search_query );
//print( $search_result->num_rows );
if( $search_result->num_rows == 0 ) {
	$insert_query = 'insert into page_count ( pagename, count ) '
		. "values( \"$this_page\", 1 )";
	$insert_result = $db->query( $insert_query );
	print $insert_query;
}

$count_query = 'select count from page_count '
	. "where pagename = \"$this_page\"";
$count_result = $db->query( $count_query );
$count_row = $count_result->fetch_object();
$count = $count_row->count;

$update_query = 'update page_count '
	. "set count = " . ($count + 1)
	. " where pagename = \"$this_page\"";
//print "<pre>" . $update_query . "</pre>\n";
$update_result = $db->query( $update_query );

?>
```

We used this in `message.php` to count the number of hits on that page.  Notice that by including `count_updater.php`, the PHP variable `$_SERVER[ 'PHP_SELF' ]` relates to `message.php`, which is precisely the action we wanted.

```php
<html>
	<head>
		<title>Hello</title>
	</head>
	<body>
	<?php
	require_once( './.htpasswd' );

	include_once( 'count_updater.php' );

	?>
		<p>Hello from ITE 254!</p>
		<div id="bottom">
			<p>This page has been visited
			<?php echo $count; ?>
			times.</p>
		</div>
	</body>
</html>
```

One thing that I didn't mention in class is the use of `require_once()` instead of `require()` (or `include_once()` instead of `include()`).  By doing this, even if the line `require_once( './.htpasswd' )` were to appear in both files, `.htpasswd` would only be processed once.