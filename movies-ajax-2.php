<html>
    <head>
        <title>Some AJAX Stuff</title>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    </head>
    <body>
    <?php
        require_once( '.htpasswd' );  
    ?>
    <input type="text" id="name" />
        <div id="details"></div>
        
    <script type="text/javascript">
        $(function(){
            $('input#name').on('keyup',function(){
                var name = $('input#name').val();
                //console.log(name);
                $.post( 'partial-title.php',
                      { 'title': name },
                        function(data){
                    $('div#details').html(data);
                }
                      )
            })
        })
    </script>
        
    </body>
</html>