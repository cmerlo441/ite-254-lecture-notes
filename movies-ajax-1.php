<html>
    <head>
        <title>Some AJAX Stuff</title>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    </head>
    <body>
    <?php
        require_once( '.htpasswd' );
        print( "List of movie titles:" );
        $titles_query = "select id, title, year "
            . "from movies "
            . "order by title";
        $titles_result = $db->query( $titles_query );
        print "<ul>\n";
        while( $movie = $titles_result->fetch_object() ) {
            print "<li id=\"$movie->id\">"
                . "<a href=\"javascript:void(0)\">$movie->title</a></li>\n";
        }
        print "</ul>\n";
        
        
        print "<div id=\"movie-details\"></div>\n";
    ?>
        
    <script type="text/javascript">
    $(function(){
        $('a').click(function(){
            var id = $(this).parent().attr('id');
            //alert(id);
            //$('div#movie-details').html(id);
            $.post('movie-details.php',
                { 'id' : id },
                function(data){
                    $('div#movie-details').html(data);
                }
            )
        })
    })
    </script>
        
    </body>
</html>